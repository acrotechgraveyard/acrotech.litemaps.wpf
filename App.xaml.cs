﻿using System;
using System.Windows;
using Acrotech.LiteMaps.Engine;
using Acrotech.LiteMaps.Engine.Storage;
using Acrotech.LiteMaps.WPF.Storage;
using Acrotech.PortableIoCAdapter;
using Acrotech.PortableIoCAdapter.Containers;
using Acrotech.PortableLogAdapter;

namespace Acrotech.LiteMaps.WPF
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            Container = new SimpleContainer();
            LogManager = LogAdapter.NLogManager.Default;

            Container.Register<ILogManager>(LogManager);
            Container.Register<ITileStorage>(LocalTileStorage.Default);

            Services.Initialize(Container);
        }

        public static IContainer Container { get; private set; }
        public static ILogManager LogManager { get; private set; }

        public static bool IsInDesignMode
        {
            get { return (bool)System.ComponentModel.DesignerProperties.IsInDesignModeProperty.GetMetadata(typeof(DependencyObject)).DefaultValue; }
        }

        public static T DesignModeSafe<T>(Func<T> action, T defaultValue = default(T))
        {
            T value = defaultValue;

            if (IsInDesignMode == false)
            {
                value = action();
            }

            return value;
        }
    }
}
