﻿using System.Globalization;
using System.Threading;
using System.Windows;
using System.Windows.Media;
using Acrotech.LiteMaps.Engine;

namespace Acrotech.LiteMaps.WPF.Layers
{
    public class FPSLayer : WPFLayer
    {
        public const string Name = "FPS";

        public FPSLayer(MapViewPort viewPort)
            : base(new LayerViewModel(Name))
        {
            ViewPort = viewPort;

            ViewModel.IsEnabled = true;

            WaitHandle = new ManualResetEventSlim(false);

            ThreadPool.QueueUserWorkItem(_ => RenderCountWatcher());
        }

        public MapViewPort ViewPort { get; private set; }

        private ManualResetEventSlim WaitHandle { get; set; }

        public int Counter { get; private set; }
        public int FPS { get; private set; }

        private void RenderCountWatcher()
        {
            while (WaitHandle.Wait(1000) == false)
            {
                FPS = Counter;
                Counter = 0;

                ViewPort.RefreshMapAsync();
            }
        }

        public override void OnRender(DrawingContext dc, ViewPortViewModel viewModel, MapViewPort viewPort)
        {
            base.OnRender(dc, viewModel, viewPort);

            ++Counter;

            if (ViewModel.IsEnabled)
            {
                var txt = new FormattedText(string.Format("{0} FPS", FPS), CultureInfo.CurrentUICulture, FlowDirection.LeftToRight, MapViewPort.DebugTypeface, 12, Brushes.White)
                {
                    TextAlignment = TextAlignment.Center
                };

                var bounds = new Rect(viewPort.RenderSize.Width - 100, 0, 100, 25);

                dc.DrawRectangle(MapViewPort.DebugTextBackgroundBrush, null, bounds);

                var p = new Point(bounds.X + bounds.Width / 2, bounds.Y + 5);

                dc.DrawText(txt, p);
            }
        }
    }
}
