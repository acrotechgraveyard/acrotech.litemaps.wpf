﻿using System;
using System.IO;
using System.Linq;
using Acrotech.LiteMaps.Engine;
using Acrotech.LiteMaps.Engine.Sources;
using Acrotech.LiteMaps.Engine.Storage;
using Acrotech.PortableLogAdapter;

namespace Acrotech.LiteMaps.WPF.Storage
{
    public class LocalTileStorage : ITileStorage
    {
        private static readonly ILogger Logger = App.LogManager.GetLogger<LocalTileStorage>();

        public static readonly LocalTileStorage Default = new LocalTileStorage(new DirectoryInfo(Path.Combine(Directory.GetCurrentDirectory(), "Tiles")));

        public LocalTileStorage(DirectoryInfo localStorage)
        {
            LocalStorage = localStorage;
        }

        public DirectoryInfo LocalStorage { get; private set; }

        public virtual FileInfo GetImageFile(TileImageSource source, Tile tile)
        {
            var x = tile.X;
            var y = tile.Y;

            Tile.Normalize(ref x, ref y, tile.Zoom);

            return LocalStorage == null ? null : new FileInfo(Path.Combine(LocalStorage.FullName, source.Key, tile.Zoom.ToString(), string.Format("{0}_{1}.{2}", x, y, source.ImageFormat)));
        }

        public static void CreateDirectory(FileInfo file)
        {
            if (file != null && file.Directory.Exists == false)
            {
                Logger.Debug("Creating Directory: {0}", file.Directory.FullName);

                file.Directory.Create();
            }
        }

        #region ITileStorage Members

        public bool SaveTileImage(TileImageSource source, Tile tile, byte[] content)
        {
            var saved = false;

            try
            {
                if (content != null && content.Any())
                {
                    var file = GetImageFile(source, tile);

                    if (file != null)
                    {
                        CreateDirectory(file);

                        Logger.Debug("Saving Image File... {0}", file.FullName);

                        using (var fs = file.OpenWrite())
                        {
                            fs.Write(content, 0, content.Length);
                        }

                        saved = true;

                        Logger.Info("Image File Saved: {0}", file.FullName);
                    }
                }
                else
                {
                    Logger.Warn("Image Content is Empty");
                }
            }
            catch (Exception e)
            {
                Logger.Warn("Unable to Save Image File", e);
            }

            return saved;
        }

        public byte[] GetTileImage(Uri uri)
        {
            byte[] content = null;

            if (uri.IsFile)
            {
                var file = new FileInfo(uri.LocalPath);

                if (file.Exists && file.Length > 0)
                {
                    using (var fs = file.OpenRead())
                    {
                        content = new byte[file.Length];

                        fs.Read(content, 0, content.Length);
                    }
                }
            }

            return content;
        }

        public Uri GetStorageUri(TileImageSource source, Tile tile)
        {
            Uri uri = null;

            var file = GetImageFile(source, tile);

            if (file != null && file.Exists)
            {
                uri = new Uri(file.FullName);
            }

            return uri;
        }

        #endregion
    }
}
