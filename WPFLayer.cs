﻿using System.Windows.Media;
using Acrotech.LiteMaps.Engine;
using Acrotech.LiteMaps.WPF;

namespace Acrotech.LiteMaps
{
    public abstract class WPFLayer
    {
        public WPFLayer(LayerViewModel viewModel)
        {
            ViewModel = viewModel;
        }

        public LayerViewModel ViewModel { get; private set; }

        public virtual void OnRender(DrawingContext dc, ViewPortViewModel viewModel, MapViewPort viewPort)
        {
            // do nothing
        }
    }
}
