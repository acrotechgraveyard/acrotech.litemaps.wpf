﻿using System.Windows;
using Acrotech.LiteMaps.Engine;
using Acrotech.LiteMaps.WPF.Layers;

namespace Acrotech.LiteMaps.WPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            Map.DataContext = new MapViewModel();

            Map.AddLayer(new FPSLayer(Map.ViewPort));
        }
    }
}
