﻿using System;
using System.Windows;
using System.Windows.Data;

namespace Acrotech.LiteMaps.WPF.Converters
{
    public class BoolToVisibilityConverter : IValueConverter
    {
        public static readonly BoolToVisibilityConverter TrueToVisibleConverter = new BoolToVisibilityConverter(Visibility.Visible, Visibility.Collapsed);
        public static readonly BoolToVisibilityConverter FalseToVisibleConverter = new BoolToVisibilityConverter(Visibility.Collapsed, Visibility.Visible);

        public BoolToVisibilityConverter(Visibility trueVisibility, Visibility falseVisibility)
        {
            TrueVisibility = trueVisibility;
            FalseVisibility = falseVisibility;
        }

        public Visibility TrueVisibility { get; private set; }
        public Visibility FalseVisibility { get; private set; }

        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var result = Visibility.Visible;

            if (value is bool)
            {
                var boolValue = (bool)value;

                result = boolValue ? TrueVisibility : FalseVisibility;
            }

            return result;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
