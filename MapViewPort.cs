﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Acrotech.LiteMaps.Engine;
using Acrotech.LiteMaps.Engine.Sources;
using Acrotech.LiteMaps.Engine.Storage;
using Acrotech.PortableLogAdapter;
using NLog;

namespace Acrotech.LiteMaps.WPF
{
    public class MapViewPort : Panel
    {
        private static readonly ILogger Logger = App.DesignModeSafe(() => App.LogManager.GetLogger<MapViewPort>());

        public static readonly Brush DebugTextBackgroundBrush = new SolidColorBrush(Color.FromArgb(100, 100, 100, 100));
        public static readonly Pen GridPen = new Pen(Brushes.White, 1d);
        public static readonly Pen CrossPen = new Pen(DebugTextBackgroundBrush, 1d);
        public static readonly Typeface DebugTypeface = new Typeface("Courier New");

        private static readonly TaskScheduler UITaskScheduler = TaskScheduler.FromCurrentSynchronizationContext();

        public MapViewPort()
        {
            
            DataContextChanged += (s, e) => Initialize();

            Images = new Dictionary<string, ImageSource>();
            Layers = new Dictionary<Guid, WPFLayer>();
            WaitHandle = new ManualResetEventSlim(false);
        }

        public ViewPortViewModel ViewModel { get { return (ViewPortViewModel)DataContext; } }
        public ITileStorage Storage { get; private set; }

        public Dictionary<string, ImageSource> Images { get; private set; }
        public Dictionary<Guid, WPFLayer> Layers { get; private set; }

        private ManualResetEventSlim WaitHandle { get; set; }

        protected virtual void Initialize()
        {
            if (ViewModel != null)
            {
                ViewModel.PropertyChanged += ViewModel_PropertyChanged;
                ViewModel.Map.TileImageSource.Downloader.TileDownloaded += (s, t) => RefreshMapAsync();

                Storage = ViewModel.Map.TileImageSource.Storage;

                OnRenderSizeChanged(null);

                var viewModel = ViewModel;
                ThreadPool.QueueUserWorkItem(_ => ImageLoader(viewModel));
            }
        }

        protected virtual void ViewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Tiles")
            {
                RefreshMapAsync();
            }
        }

        public Task RefreshMapAsync()
        {
            return Task.Factory.StartNew(RefreshMap, CancellationToken.None, TaskCreationOptions.None, UITaskScheduler);
        }

        public virtual void RefreshMap()
        {
            InvalidateVisual();
        }

        protected override void OnRenderSizeChanged(System.Windows.SizeChangedInfo sizeInfo)
        {
            if (sizeInfo != null)
            {
                base.OnRenderSizeChanged(sizeInfo);
            }

            if (RenderSize.IsEmpty == false && ViewModel != null)
            {
                ViewModel.Resize((int)RenderSize.Width, (int)RenderSize.Height);
            }
        }

        protected override void OnRender(DrawingContext dc)
        {
            base.OnRender(dc);

            if (ViewModel != null)
            {
                if (RenderSize.IsEmpty == false)
                {
                    dc.DrawRectangle(Brushes.Black, null, new Rect(0, 0, RenderSize.Width, RenderSize.Height));

                    var tiles = ViewModel.Tiles;

                    if (tiles != null)
                    {
                        var tileSize = ViewModel.Map.TileImageSource.TileSize;

                        ImageSource image = null;

                        foreach (var tile in tiles)
                        {
                            var bounds = new Rect(tile.X + 0.5, tile.Y + 0.5, tileSize, tileSize);

                            if (Images.TryGetValue(tile.Tile.Key, out image))
                            {
                                dc.DrawImage(image, bounds);
                            }

                            if (ViewModel.IsTileGridVisible)
                            {
                                dc.DrawRectangle(null, GridPen, bounds);
                            }

                            if (ViewModel.IsTileDebugTextVisible)
                            {
                                dc.DrawRectangle(DebugTextBackgroundBrush, null, bounds);

                                var txt = new FormattedText(tile.ToString(), CultureInfo.CurrentUICulture, FlowDirection.LeftToRight, DebugTypeface, 12, Brushes.White)
                                {
                                    MaxTextWidth = tileSize,
                                    MaxTextHeight = tileSize,
                                    TextAlignment = TextAlignment.Center
                                };

                                var p = new Point(bounds.X, bounds.Y + tileSize / 2 - txt.Height / 2);

                                dc.DrawText(txt, p);
                            }
                        }

                        foreach (var layer in ViewModel.Map.EnabledLayers.Select(x => GetLayer(x)).Where(x => x != null))
                        {
                            layer.OnRender(dc, ViewModel, this);
                        }
                    }

                    if (ViewModel.IsCrossVisible)
                    {
                        var x = RenderSize.Width / 2 + 0.5;
                        var y = RenderSize.Height / 2 + 0.5;

                        dc.DrawLine(CrossPen, new Point(x, 0), new Point(x, RenderSize.Height));
                        dc.DrawLine(CrossPen, new Point(0, y), new Point(RenderSize.Width, y));
                    }
                }
            }
        }

        protected virtual void ImageLoader(ViewPortViewModel viewModel)
        {
            while (WaitHandle.Wait(0) == false)
            {
                var isUpdated = viewModel.UpdateImages(Images, CreateFrozenImageSource);

                if (isUpdated == false)
                {
                    WaitHandle.Wait(100);
                }
                else
                {
                    RefreshMapAsync();
                }
            }
        }

        protected virtual ImageSource CreateFrozenImageSource(Uri uri)
        {
            ImageSource image = null;

            if (uri.IsFile)
            {
                image = CreateFileImageSource(uri);
            }
            else if (uri.Scheme == CacheTileStorage.BaseUri.Scheme)
            {
                image = CreateCacheImageSource(uri);
            }
            else
            {
                Logger.Warn("Unrecognized Uri Scheme: {0}", uri);
            }

            if (image != null)
            {
                image.Freeze();
            }

            return image;
        }

        private ImageSource CreateFileImageSource(Uri uri)
        {
            return new BitmapImage(uri);
        }

        private ImageSource CreateCacheImageSource(Uri uri)
        {
            ImageSource image = null;

            var content = Storage.GetTileImage(uri);

            if (content != null && content.Length > 0)
            {
                using (var ms = new MemoryStream(content))
                {
                    var bmp = new BitmapImage();

                    bmp.BeginInit();
                    bmp.CacheOption = BitmapCacheOption.OnLoad;
                    bmp.StreamSource = ms;
                    bmp.EndInit();

                    image = bmp;
                }
            }

            return image;
        }

        protected virtual WPFLayer GetLayer(LayerViewModel viewModel)
        {
            WPFLayer layer = null;

            Layers.TryGetValue(viewModel.Guid, out layer);

            return layer;
        }

        public virtual void AddLayer(WPFLayer layer)
        {
            Layers.Add(layer.ViewModel.Guid, layer);
        }

        #region Mouse Handling

        private Point DragOrigin { get; set; }

        protected override void OnMouseLeftButtonDown(MouseButtonEventArgs e)
        {
            base.OnMouseLeftButtonDown(e);

            DragOrigin = e.GetPosition(this);

            CaptureMouse();
        }

        protected override void OnMouseLeftButtonUp(MouseButtonEventArgs e)
        {
            ReleaseMouseCapture();
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            if (IsMouseCaptured)
            {
                var v = DragOrigin - e.GetPosition(this);

                ViewModel.Pan((int)v.X, (int)v.Y);

                DragOrigin = e.GetPosition(this);
            }
        }

        protected override void OnMouseWheel(MouseWheelEventArgs e)
        {
            if (e.Delta > 0)
            {
                if (ViewModel.Map.ZoomIn.CanExecute())
                {
                    ViewModel.Map.ZoomIn.Execute();
                }
            }
            else if (e.Delta < 0)
            {
                if (ViewModel.Map.ZoomOut.CanExecute())
                {
                    ViewModel.Map.ZoomOut.Execute();
                }
            }
        }

        #endregion
    }
}
