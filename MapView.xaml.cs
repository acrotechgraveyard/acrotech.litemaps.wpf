﻿using System.Windows.Controls;
using Acrotech.LiteMaps.Engine;

namespace Acrotech.LiteMaps.WPF
{
    /// <summary>
    /// Interaction logic for MapView.xaml
    /// </summary>
    public partial class MapView : UserControl
    {
        public MapView()
        {
            InitializeComponent();
        }

        public MapViewModel ViewModel { get { return (MapViewModel)DataContext; } }

        public virtual void AddLayer(WPFLayer layer)
        {
            ViewPort.AddLayer(layer);

            ViewModel.Layers.Add(layer.ViewModel);
        }
    }
}
