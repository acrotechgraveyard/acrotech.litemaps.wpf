﻿using Acrotech.PortableLogAdapter;

namespace Acrotech.LiteMaps.WPF.LogAdapter
{
    public class NLogManager : ILogManager
    {
        public static readonly NLogManager Default = new NLogManager();

        private NLogManager()
        {
        }

        public ILogger GetLogger(string name)
        {
            return new NLogLogger(NLog.LogManager.GetLogger(name));
        }
    }
}
