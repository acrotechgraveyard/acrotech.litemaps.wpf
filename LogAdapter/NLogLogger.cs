﻿using System;
using Acrotech.PortableLogAdapter;

namespace Acrotech.LiteMaps.WPF.LogAdapter
{
    public class NLogLogger : ILogger
    {
        public NLogLogger(NLog.Logger logger)
        {
            Logger = logger;
        }

        public NLog.Logger Logger { get; private set; }

        public string Name { get { return Logger.Name; } }

        public void Log(LogLevel level, string format, params object[] args)
        {
            Logger.Log(Convert(level), format, args);
        }

        public void Log(LogLevel level, Func<string> messageCreator)
        {
            Logger.Log(Convert(level), new NLog.LogMessageGenerator(messageCreator));
        }

        public void LogException(LogLevel level, Exception exception, string format, params object[] args)
        {
            Logger.Log(Convert(level), format.FormatSafe(args), exception);
        }

        public void LogException(LogLevel level, Exception exception, Func<string> messageCreator)
        {
            Logger.Log(Convert(level), messageCreator(), exception);
        }

        public bool IsEnabled(LogLevel level)
        {
            return Logger.IsEnabled(Convert(level));
        }

        public static NLog.LogLevel Convert(LogLevel level)
        {
            return NLog.LogLevel.FromOrdinal((int)level);
        }
    }
}
